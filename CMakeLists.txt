cmake_minimum_required(VERSION 3.16)
project(qqc2-desktop-style)

set(CMAKE_AUTOMOC ON)

find_package(Qt6 REQUIRED NO_MODULE COMPONENTS Core Quick)

add_subdirectory(org/kde/qqc2desktopstyle/private)
add_subdirectory(org/kde/desktop)
