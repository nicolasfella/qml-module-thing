#ifndef KQUICKSTYLEITEM_P_H
#define KQUICKSTYLEITEM_P_H

#include <QObject>
#include <qqmlregistration.h>


class KQuickStyleItem : public QObject
{
    Q_OBJECT
    QML_NAMED_ELEMENT(StyleItem)
};

#endif
