qt6_add_qml_module(org_kde_desktop URI "org.kde.desktop" VERSION 1.0 DEPENDENCIES "org.kde.qqc2desktopstyle.private" QML_FILES HorizontalHeaderView.qml)

set_target_properties(org_kde_desktop PROPERTIES
    QT_QMLCACHEGEN_ARGUMENTS "--verbose"
)
